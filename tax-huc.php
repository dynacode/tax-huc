<?php
/*
Plugin Name: Hollywood Urgent Care 
Version: 0.1
Description: Hollywood Urgent Care
Author: Juan Sebastián Echeverry
Text Domain: tax-huc

Copyright 2016 Juan Sebastián Echeverry (baxtian.echeverry@gmail.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// Do not active this plugin if timber isn't installed 
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( !is_plugin_active('timber-library/timber.php') ) {
    add_action('admin_notices', function() { 
		echo '<div id="message" class="error fade"><p style="line-height: 150%">';
		_e('The <strong>Hollywood Walk-In Clinic</strong> plugin requieres <strong>Timber</strong>', 'huc');
		echo '</p></div>';
	});
    return;
}

define("HWIC_V", 0.01);

add_action( 'init', 'huc_taxonomy' );
add_action( 'admin_init', 'huc_adminheader');

add_action( 'save_post_service', 'huc_save_price' );
add_action( 'save_post_infusion', 'huc_save_price' );

add_action( 'issue_add_form_fields' , 'huc_box_tax_price' );
add_action( 'issue_edit_form_fields', 'huc_box_tax_price' );
add_action( 'edit_issue',   'huc_save_tax_price' );
add_action( 'create_issue', 'huc_save_tax_price' );

add_action( 'issue_add_form_fields' , 'huc_box_tax_display' );
add_action( 'issue_edit_form_fields', 'huc_box_tax_display' );
add_action( 'edit_issue',   'huc_save_tax_display' );
add_action( 'create_issue', 'huc_save_tax_display' );

// Declare styles and scripts into admin pages
function huc_adminheader() {
	wp_register_style('huc', plugin_dir_url(__FILE__)."css/admin.css", array(), HWIC_V);
	wp_enqueue_style('huc');
}

// Declare custom post types and taxonomies
function huc_taxonomy() {

	//Declare 'Service' as a new post type
	$labels = array(
		'name' => __('Services', 'huc'),
		'singular_name' => __('Service', 'huc'),
		'add_new' => __('Add new', 'huc'),
		'add_new_item' => __('Add new Service', 'huc'),
		'edit_item' => __('Edit Service', 'huc'),
		'new_item' => __('New Service', 'huc'),
		'view_item' => __('View Service', 'huc'),
		'search_items' => __('Search Services', 'huc'),
		'not_found' =>  __('No services found', 'huc'),
		'not_found_in_trash' => __('No services found in trash', 'huc'),
		'parent_item_colon' => '',
		'menu_name' => __('Services', 'huc'),
	);
	
	$args = array(
		'query_var' => true,
		'labels' => $labels,
		'public' => true,
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => 5,
		'taxonomies' => array(  ),
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
		'rewrite' => array( 'slug' => __('service', 'huc') )
	);
	register_post_type('service',$args);

	//Declare 'Infusion' as a new post type
	$labels = array(
		'name' => __('Infusions', 'huc'),
		'singular_name' => __('Infusion', 'huc'),
		'add_new' => __('Add new', 'huc'),
		'add_new_item' => __('Add new Infusion', 'huc'),
		'edit_item' => __('Edit Infusion', 'huc'),
		'new_item' => __('New Infusion', 'huc'),
		'view_item' => __('View Infusion', 'huc'),
		'search_items' => __('Search Infusions', 'huc'),
		'not_found' =>  __('No infusions found', 'huc'),
		'not_found_in_trash' => __('No infusions found in trash', 'huc'),
		'parent_item_colon' => '',
		'menu_name' => __('Infusions', 'huc'),
	);
	
	$args = array(
		'query_var' => true,
		'labels' => $labels,
		'public' => true,
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => 5,
		'taxonomies' => array(  ),
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
		'rewrite' => array( 'slug' => __('infusion', 'huc') )
	);
	register_post_type('infusion',$args);

	// Declare taxonomy 'issue'
	$labels = array(
		'name' => __('Issues', 'huc'),
		'singular_name' => __('Issue', 'huc'),
	    'search_items' => __('Search Issues', 'huc'),
	    'all_items' => __('All Issues', 'huc'),
	    'edit_item' => __('Edit Issue', 'huc'),
	    'update_item' => __('Update Issue', 'huc'),
	    'add_new_item' => __('Add New Issue', 'huc'),
	    'new_item_name' => __('New Issue Name', 'huc'),
	    'menu_name' => __('Issues', 'huc'),
		'popular_items' => __('Popular Issue', 'huc'),
		'add_or_remove_items' => __('Add or remove issue', 'huc'),
		'parent_item' => null, 'parent_item_colon' => null,
		'choose_from_most_used' => __('Choose from the most used issues', 'huc'),
		'separate_items_with_commas' => __( 'Separate issues with commas', 'huc' ),
	);

	register_taxonomy('issue',array('service', 'infusion'), array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => __('issue', 'huc') )
	));
	
	// Enlace a función que crea las cajas donde estarán las entradas de
	// las variables
	add_action( 'add_meta_boxes', 'huc_boxes' );

}

// Function to create boxes
function huc_boxes() {

	//Special box for infusion price
	add_meta_box( 
        'price',
        __('Price', 'huc'),
        'huc_box_price',
        'infusion' 
    );
    
    //Special box for service price
	add_meta_box( 
        'price',
        __('Price', 'huc'),
        'huc_box_price',
        'service' 
    );

}

// Box for price
function huc_box_price() {
	global $post;
	
	// Use nonce for verification
	wp_nonce_field( plugin_basename( __FILE__ ), 'nonce_huc' );

	// Get data
	$price = get_post_meta($post->ID,'_price',true);
	
	// Html code
	echo "<label for='price'>" . __('Price', 'huc') . ":</label>";
	echo "<input type='text' id='price' name='price' value='{$price}'/>";

}

// Save price
function huc_save_price( $post_id ) {
	//Verify nonce field
	if(!wp_verify_nonce( $_POST['nonce_huc'], plugin_basename( __FILE__ ) )) return;
	
	update_post_meta( $post_id, '_price', sanitize_text_field($_POST['price']) );
}

// Tax box display
function huc_box_tax_display( $term ) {
		
	$args = array();
	//If there is an object is because we are editing
	if(is_object($term)) {
		//Data
		$args['display'] = get_term_meta( $term->term_id, 'display', true );
		$args['edit'] = true;
	}

	// Twig		
	huc_render("taxterms-display.tpl", $args);    

}

// Save tax box display
function huc_save_tax_display( $term_id ) {
		
	update_term_meta( $term_id, 'display', isset($_POST['display']) );
}

// Tax box price
function huc_box_tax_price( $term ) {
		
	$args = array();
	//If there is an object is because we are editing
	if(is_object($term)) {
		//Data
		$args['price'] = get_term_meta( $term->term_id, 'price', true );
		$args['edit'] = true;
	}

	// Twig		
	huc_render("taxterms-price.tpl", $args);    

}

// Save tax box price
function huc_save_tax_price( $term_id ) {
		
	delete_term_meta( $term_id, 'price' );
	
	if(isset($_POST['price'])) {
		update_term_meta( $term_id, 'price', $_POST['price'] );
	}
}

// Function to add columns to the issues edit list
function huc_issue_columns( $columns ) {
	$pos   = count($columns) - 1;
	$columns = array_merge(
		array_slice($columns, 0, $pos),
		array('price' => __('Price', 'huc'), 'display' => __('Show in Global List', 'huc')),
		array_slice($columns, $pos)
	);

	return $columns;
}
add_filter('manage_edit-issue_columns' , 'huc_issue_columns');

//Function to manage new issue's columns
function huc_issue_columns_content( $content, $column_name, $term_id ) {
    if ( 'price' == $column_name ) {
		$content = get_term_meta( $term_id, 'price', true );
    } elseif ( 'display' == $column_name ) {
		$content = (get_term_meta( $term_id, 'display', true )) ? "<i class='dashicons dashicons-yes'></i>" : "";
	}
	return $content;
}
add_filter( 'manage_issue_custom_column', 'huc_issue_columns_content', 10, 3 );

// Function to render
function huc_render( $tpl, $args, $echo = true ) {
	$context = Timber::get_context();
	$context['plugin'] = array( 'directory' => plugin_dir_url(__FILE__) );
	Timber::$dirname = 'templates';
	
	//Render
	if($echo) return Timber::render($tpl, array_merge($context, $args));
	// o feed?
	return Timber::fetch($tpl, array_merge($context, $args));
	
}

?>
