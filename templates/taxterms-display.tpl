{% if edit %}
<tr class="form-field jt-term-display-wrap">
	<th scope="row"><label for="jt-term-display">{{ __('Show in Global List', 'huc') }}:</label></th>
	<td>
		<input type="checkbox" name="display" {{ function('checked', display, true, false) }}></label>
	</td>
</tr>
{% else %}
<div class='form-field term-image-wrap'>
	<label for='display'>{{ __('Show in Global List', 'huc') }}</label>
	<input id='display' value='' type='checkbox' name='display'/>
	<!--<p class="description">Price.</p>-->
</div>
{% endif %}
