{% if edit %}
<tr class="form-field jt-term-price-wrap">
	<th scope="row"><label for="jt-term-price">{{ __('Price', 'huc') }}:</label></th>
	<td>
		<input type="text" name="price" value="{{ price }}"></label>
	</td>
</tr>
{% else %}
<div class='form-field term-image-wrap'>
	<label for='price'>{{ __('Price', 'huc') }}</label>
	<input id='price' value='' size='40' type='text' name='price'/>
	<!--<p class="description">Price.</p>-->
</div>
{% endif %}
